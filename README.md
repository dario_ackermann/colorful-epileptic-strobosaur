# Colorful Epileptic Strobosaur

This is a gamejam fork of the t-rex-runner found [here](https://github.com/wayou/t-rex-runner.git)

## Key features

* Stroboscope effect
* Local Multiplayer (1vs1)

## License

The BSD License applies to the original code of the chromium authors as well
as any modifications made in this fork.