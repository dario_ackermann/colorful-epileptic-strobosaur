/**
 * Sync Hack for 2 runners
 * (C) 2019 Dario Ackermann
 * BSD License
 */
window.addEventListener("keydown", (e)=>{

    if(e.keyCode === 32 && window.r1.crashed && window.r2.crashed) {
        r1.restart()
        r2.restart()
    }
});
