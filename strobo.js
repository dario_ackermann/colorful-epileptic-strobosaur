/**
 * Strobo Hack
 * (C) 2019 Jonas Luther
 * BSD License
 */
if(window.activateStrobo) {
    window.addEventListener("load", () => {
        let i = 0;
        setInterval(() => {
            document.body.style.backgroundColor = ["#FF0000", "#00FF00", "#0000FF"][i++ % 3];
        }, 10)
    });
}